<?php 
//namespace bitm;
?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Web Apps Develop PHP</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="bootstrap-3.3.6/css/bootstrap.min.css">
        
        <script src="jquery/jquery.js"></script>
        
        <script src="bootstrap-3.3.6/js/bootstrap.min.js"></script>
    </head>
    
    
    <body>
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 lead">
                    <h1 class="">BITM 5th Class : 15-Feb-16</h1>
                    
                    <?php
                    
                        define ('Title', 'BITM-B16');                        
                        echo 'Title <br>';
                        echo 'BITM-B16';
                    
                    echo   '<br>-------------------------------------------- ' ;
                    
//                        magic constant                    
                            echo '<br>line number is : '.__LINE__;
                            
//                            echo '<br>This is line of 5th php class.' ._LINE_;
                            
                            echo '<br>Directory is : '.__DIR__;
                            
                            echo '<br>File is : '.__FILE__;
                            
                            echo   '<br>-------------------------------------------- ' ;
                            
                            function addition($a, $b){
                                $z = $a + $b;
                                echo  '<br>' . $z . ' ' ;
                                echo __FUNCTION__;
                            }
                            
                            addition(3, 5);
                            
//                            echo '<br> the namespace is : ' . __NAMESPACE__ . '<br>';
                            
                            echo   '<br>-------------------------------------------- <br>' ;
                            
                            
                            class testing{

                                public function test(){
                                    echo __CLASS__;
                                }
                                
                                public function test2(){
                                    echo __METHOD__;
                                }
                            }
                            
                            $test_obj = new testing();
                            
                            $test_obj ->test();
                            $test_obj ->test2();
                    
                    ?>
                    
                </div>
            </div>
        </div>        
        
    </body>
</html>
